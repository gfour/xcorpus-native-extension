This repository converts XCorpus benchmarks for Doop
analysis. Currently the following benchmarks are supported: aspectj,
lucene, tomcat.

Setup:

(1) Clone the original XCorpus repository
(https://bitbucket.org/jensdietrich/xcorpus/) and set environment
variable `XCORPUS_DIR` to point to its location in the filesystem.

(2) Run: `./repack-native-benchmarks.sh`

The resulting inputs are under "repackaged". The native inputs are
under "native".
