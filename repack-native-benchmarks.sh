#!/usr/bin/env bash

function initRepackDir() {
    export EXTRACT_DIR=${XCORPUS_EXT}/repackaged/$1/extracted
    deleteRepackDir
}

function deleteRepackDir() {
    echo "Deleting: ${EXTRACT_DIR}"
    rm -rf "${EXTRACT_DIR}"
}

function repackAspectJ() {
    initRepackDir "aspectj-1.6.9"

    cd ${XCORPUS_DIR}/data/qualitas_corpus_20130901/aspectj-1.6.9/project
    echo "* Processing project code in ${PWD}"
    ${XCORPUS_EXT}/repack-standalone-jars.sh bin.zip aspectjrt1.6.9 aspectjtools1.6.9 aspectjweaver1.6.9 org.aspectj.matcher-1.6.9

    cd ${XCORPUS_DIR}/data/qualitas_corpus_20130901/aspectj-1.6.9/project
    echo "* Processing .xcorpus code in ${PWD}"
    ${XCORPUS_EXT}/repack-standalone-jars.sh evosuite-tests.zip aspectjrt1.6.9 aspectjtools1.6.9 aspectjweaver1.6.9 org.aspectj.matcher-1.6.9

    deleteRepackDir
}

function repackLucene() {
    initRepackDir "lucene-4.3.0"

    cd ${XCORPUS_DIR}/data/qualitas_corpus_20130901/lucene-4.3.0/project
    echo "* Processing project code in ${PWD}"
    ${XCORPUS_EXT}/repack-standalone-jars.sh bin.zip lucene-analyzers-common-4.3.0 lucene-analyzers-icu-4.3.0 lucene-analyzers-kuromoji-4.3.0 lucene-analyzers-morfologik-4.3.0 lucene-analyzers-phonetic-4.3.0 lucene-analyzers-smartcn-4.3.0 lucene-analyzers-stempel-4.3.0 lucene-analyzers-uima-4.3.0 lucene-benchmark-4.3.0 lucene-classification-4.3.0 lucene-codecs-4.3.0 lucene-core-4.3.0 lucene-demo-4.3.0 lucene-facet-4.3.0 lucene-grouping-4.3.0 lucene-highlighter-4.3.0 lucene-join-4.3.0 lucene-memory-4.3.0 lucene-misc-4.3.0 lucene-queries-4.3.0 lucene-queryparser-4.3.0 lucene-sandbox-4.3.0 lucene-spatial-4.3.0 lucene-suggest-4.3.0 lucene-test-framework-4.3.0
    ${XCORPUS_EXT}/repack-standalone-jars.sh builtin-tests.zip lucene-analyzers-common lucene-analyzers-icu lucene-analyzers-kuromoji lucene-analyzers-morfologik lucene-analyzers-phonetic lucene-analyzers-smartcn lucene-analyzers-stempel lucene-analyzers-uima lucene-benchmark lucene-classification lucene-codecs lucene-core lucene-demo lucene-facet lucene-grouping lucene-highlighter lucene-join lucene-memory lucene-misc lucene-queries lucene-queryparser lucene-sandbox lucene-spatial lucene-suggest

    cd ${XCORPUS_DIR}/data/qualitas_corpus_20130901/lucene-4.3.0/.xcorpus
    echo "* Processing .xcorpus code in ${PWD}"
    ${XCORPUS_EXT}/repack-standalone-jars.sh evosuite-tests.zip lucene-analyzers-common lucene-analyzers-icu lucene-analyzers-kuromoji lucene-analyzers-morfologik lucene-analyzers-phonetic lucene-analyzers-smartcn lucene-analyzers-stempel lucene-analyzers-uima lucene-benchmark lucene-classification lucene-codecs lucene-core lucene-demo lucene-facet lucene-grouping lucene-highlighter lucene-join lucene-memory lucene-misc lucene-queries lucene-queryparser lucene-sandbox lucene-spatial lucene-suggest lucene-test-framework

    pushd ${XCORPUS_EXT}/native/lucene/lucene-misc-4.3.0/org/apache/lucene/store
    echo "* Building native code JAR in ${PWD}"
    make jar
    popd

    deleteRepackDir
}

function repackTomcat() {
    initRepackDir "tomcat-7.0.2"

    cd ${XCORPUS_DIR}/data/qualitas_corpus_20130901/tomcat-7.0.2/project
    echo "* Processing project code in ${PWD}"
    ${XCORPUS_EXT}/repack-standalone-jars.sh bin.zip tomcat-annotations tomcat-api tomcat-bootstrap tomcat-catalina tomcat-catalina-ant tomcat-catalina-ha tomcat-catalina-tribes tomcat-coyote tomcat-dbcp tomcat-i18n-es tomcat-i18n-fr tomcat-i18n-ja tomcat-jasper tomcat-jasper-el tomcat-juli tomcat-util

    cd ${XCORPUS_DIR}/data/qualitas_corpus_20130901/tomcat-7.0.2/.xcorpus
    echo "* Processing .xcorpus code in ${PWD}"
    ${XCORPUS_EXT}/repack-standalone-jars.sh evosuite-tests.zip tomcat-catalina tomcat-catalina-ant tomcat-catalina-ha tomcat-catalina-tribes tomcat-coyote tomcat-dbcp tomcat-jasper tomcat-jasper-el tomcat-juli tomcat-util

    deleteRepackDir
}

XCORPUS_EXT=$(realpath $(dirname "$0"))

if [ "${XCORPUS_DIR}" == "" ]; then
    echo "Please set XCORPUS_DIR to point the XCorpus repo."
    exit
fi

repackAspectJ
repackLucene
repackTomcat
