#!/usr/bin/env bash

if [ "$1" == "" ] || [ "$2" == "" ]; then
    echo "Usage: repack-standalone-jars.sh ZIP [ENTRY...]"
    echo
    echo "  ZIP     The archive to read (such as 'bin.zip')."
    echo "  ENTRY   One or more top-level entries to package as separate JARs."
    echo
    echo "Environment variable EXTRACT_DIR points to the target directory to use for extraction (if empty, a default directory is used)."
    exit
fi

if [ "${EXTRACT_DIR}" == "" ]; then
    EXTRACT_DIR=$(realpath extracted)    
fi
echo "Using EXTRACT_DIR=${EXTRACT_DIR}"
if [ -d "${EXTRACT_DIR}" ]; then
    echo "WARNING: Directory ${EXTRACT_DIR} already exists."
fi

ZIP=$(realpath $1)

mkdir -p ${EXTRACT_DIR}
pushd ${EXTRACT_DIR} &> /dev/null

set -e
unzip -o ${ZIP} &> /dev/null

shift
for subproj in $*; do
    echo "Repackaging subproject as standalone JAR: ${subproj}"
    jar -cf ../${subproj}.jar -C ${subproj} .
done

popd &> /dev/null
